# Bonus Wheel v3

Bonus wheel game (updated)

## Updates:

1.) Removed all unit-testing code.

2.) All classes now inside Classes folder.

3.) Member variables initialized.

4.) Algorithm and code rewritten with scalability and readibility. Should take less than 5 seconds to add new prizes and/or change weights of prizes.

(Note: To add a prize or change weights, go to Wheel.cpp at line 30)

5.) project.android and project.ios_mac included in repository.